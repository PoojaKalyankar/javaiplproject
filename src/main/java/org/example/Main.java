package org.example;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import java.io.FileReader;
import java.io.IOException;
import java.security.KeyStore;
import java.util.*;

public class Main {
    //Number of matches played per year of all the years in IPL.
    static void matchesPlayerPerYear(List<String[]>matches) {
            HashMap<String, Integer> yearNumber = new HashMap<>();
            for (int index = 1; index < matches.size(); index++) {
                String key = (matches.get(index)[1]);
                yearNumber.put(key, yearNumber.getOrDefault(key, 0) + 1);
            }

            for (String key : yearNumber.keySet()) {
                    System.out.println(key + " : " + yearNumber.get(key));
            }
    }
    //Number of matches won of all teams over all the years of IPL.
    static void matchesWonAllTeams(List<String[]>matches){
        HashMap<String,Integer>teamNumderWon=new HashMap<>();
             for (int firstIndex = 1; firstIndex < matches.size(); firstIndex++) {
                String wonTeam=matches.get(firstIndex)[10];
                teamNumderWon.put(wonTeam,teamNumderWon.getOrDefault(wonTeam,0)+1);
            }
            for(String key:teamNumderWon.keySet()){
                if(key != "") {
                    System.out.println(key + " : " + teamNumderWon.get(key));
                }
            }
    }
    //For the year 2016 get the extra runs conceded per team.
    static  void extraRuns2016PerTeam(List<String[]>matches,List<String[]>delivary){
        HashMap<String,Integer>extraRunTeam=new HashMap<>();
        ArrayList<Integer>idsOf2016=new ArrayList<>();
        for (int matchIndex = 1; matchIndex < matches.size(); matchIndex++) {
            int year=Integer.parseInt(matches.get(matchIndex)[1]);
            int id=Integer.parseInt(matches.get(matchIndex)[0]);
            if(year==2016){
                if(!idsOf2016.contains(id)) {
                    idsOf2016.add(id);
                }
            }
        }
        for (int delivaryIndex = 1; delivaryIndex < delivary.size(); delivaryIndex++) {
            int matchid=Integer.parseInt(delivary.get(delivaryIndex)[0]);
            int extraruns=Integer.parseInt(delivary.get(delivaryIndex)[16]);
            String bowlerTeam=delivary.get(delivaryIndex)[3];
            if(idsOf2016.contains(matchid)){
                extraRunTeam.put(bowlerTeam,extraRunTeam.getOrDefault(bowlerTeam,0)+extraruns);
            }
        }
        for(String key:extraRunTeam.keySet()){
            System.out.println(key+" : "+extraRunTeam.get(key));
        }
    }
    //For the year 2015 get the top economical bowlers.
    static void top10EconomicalBowler2015(List<String[]>matches,List<String[]>delivary) {
        HashMap<String, ArrayList<Integer>> economicalBowler = new HashMap<>();
        ArrayList<Integer>idsOf2015=new ArrayList<>();
            for (int matchIndex = 1; matchIndex < matches.size(); matchIndex++) {
                int year=Integer.parseInt(matches.get(matchIndex)[1]);
                int id=Integer.parseInt(matches.get(matchIndex)[0]);
                if(year==2015){
                    idsOf2015.add(id);
                }
            }
            for(int delivaryIndex=1;delivaryIndex<delivary.size();delivaryIndex++){
                int matchId=Integer.parseInt(delivary.get(delivaryIndex)[0]);
                String bowler=delivary.get(delivaryIndex)[8];
                int runs=Integer.parseInt(delivary.get(delivaryIndex)[17]);
                if(idsOf2015.contains(matchId)){
                    if(!(economicalBowler.containsKey(bowler))){
                        ArrayList<Integer>list=new ArrayList<>();
                        list.add(runs);
                        list.add(1);
                        economicalBowler.put(bowler,list);
                    }else{
                        ArrayList<Integer>list=economicalBowler.get(bowler);
                        list.set(0,list.get(0)+runs);
                        list.set(1,list.get(1)+1);
                        economicalBowler.put(bowler,list);
                    }
                }
            }
            HashMap<String,Double>answer=new HashMap<>();
            for(String key:economicalBowler.keySet()){
               ArrayList<Integer>list=economicalBowler.get(key);
               int runs=list.get(0);
               int balls=list.get(1);
               double economy = (runs / (balls / 6.0));
               answer.put(key, economy);
            }
           List<Map.Entry<String,Double>>list=new ArrayList<>(answer.entrySet());
            Collections.sort(list, new Comparator<Map.Entry<String, Double>>() {
                @Override
                public int compare(Map.Entry<String, Double> stringDoubleEntry, Map.Entry<String, Double> t1) {
                    return stringDoubleEntry.getValue().compareTo(t1.getValue());
                }
            });

            LinkedHashMap<String,Double>sortedMap=new LinkedHashMap<>();
            for(Map.Entry<String,Double>entry:list){
                sortedMap.put(entry.getKey(), entry.getValue());
            }
            int count=0;
           for(String key:sortedMap.keySet()){
               if(count<10){
                   System.out.println(key+" : "+sortedMap.get(key));
                   count++;
               }
           }
    }

   static void teamWonBothTossMatchForEveryYear(List<String[]>matches){
        HashMap<String,Integer>teamsList=new HashMap<>();
        for(int index=1;index<matches.size();index++){
            String tossWinner=matches.get(index)[6];
            String winner=matches.get(index)[10];
            if(tossWinner.equals(winner)){
                teamsList.put(winner,teamsList.getOrDefault(winner,0)+1);
            }
        }
        System.out.println(teamsList);
   }
    static List<String[]> readCsv(String fileName){
        try (CSVReader reader = new CSVReader(new FileReader(fileName))) {
            List<String[]>converted= reader.readAll();
            return converted;
        }
        catch (IOException | CsvException e) {
            e.printStackTrace();
        }
        return  null;
    }
    public static void main(String[] args) {
        String matchesfileName = "/home/pooja/IdeaProjects/IplProject/src/main/java/org/example/matches.csv";
        String delivaryfileName1="/home/pooja/IdeaProjects/IplProject/src/main/java/org/example/deliveries.csv";

        List<String[]>matches=readCsv(matchesfileName);
        List<String[]>delivaries=readCsv(delivaryfileName1);

        //Number of matches played per year of all the years in IPL.
          matchesPlayerPerYear(matches);

        //Number of matches won of all teams over all the years of IPL.
          matchesWonAllTeams(matches);

        //For the year 2016 get the extra runs conceded per team.
          extraRuns2016PerTeam(matches,delivaries);

        //For the year 2015 get the top economical bowlers.
         top10EconomicalBowler2015(matches,delivaries);

        //Create your own scenario.
         teamWonBothTossMatchForEveryYear(matches);
    }
}


